#include<iostream>
#include<list>
#include<vector>
using namespace std;

int main() 
{	
	int n, m; //n for numbers of player , m for passes
	cout << "How many players? : ";
	cin >> n;
	cout << "Turn to eliminated : ";
	cin >> m;
	list<int> players;
	for (int i = 1; i <= n; i++) 
	{
		players.push_back(i);
	}

	list<int>::iterator potato = players.begin();
	
	vector<int> elimList;
	int elim = 0;

	//finding for winner
	while (players.size() != 1) // no winner yet
	{ 
		if (elim != m) // if potato is not at eliminate player 
		{ 	 
			if (*potato == players.back()) //if potato passed to the last player
			{  
				potato = players.begin();	//then passes to beginner player again
			}
			else 
			{
				potato++;	// potato passes to next players
			}
			elim++; // turn updated
		}
		else 
		{ // potato arrived at eliminate player
			elimList.push_back(*potato); // loser is in elimintedlist 
			potato = players.erase(potato); // player is eliminated
			if (potato == players.end())
			{
				potato = players.begin(); // start new turn
			}
			elim = 0; // reset turn
		}

	}

	cout << "The winner is "<< *potato << endl; // winner 
	system("Pause");
	return 0;
}